import { Action, ActionCreators } from '../actions';

export default function reducer(state: AppState, action: Action): AppState {
    switch (action.type) {
        case ActionCreators.FETCH_EVENTS.type:
            return { ...state, eventsList: action.payload};
        case ActionCreators.ADD_SELECTION.type:
            state.addedSelections.set(action.payload.data.id, action.payload);
            return { 
                    ...state, 
                    selectionWasAdded: true,
                    addedSelections: new Map(state.addedSelections)
                };
        case ActionCreators.REMOVE_SELECTION.type:
            
            state.addedSelections.delete(action.payload);

            return {
                    ...state, 
                    selectionWasAdded: false,
                    addedSelections: new Map(state.addedSelections)
                };
        default:
            return state;
    }

}