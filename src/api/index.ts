import { events } from './mockData';

const url = 'http://www.mocky.io/v2/59f08692310000b4130e9f71';

const fetchEvents = (): Promise<EventType[]> => {
  return Promise.resolve(events);
};

const fetchEventsAsync = (): Promise<EventType[]> => {
  return fetch(url).then((response) => (response.json()));
};

export const eventsAPI = {
  fetchEvents,
  fetchEventsAsync,
};