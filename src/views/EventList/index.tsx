import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

// Components
import Event from './components/Event';
import NavBar from '../../components/NavBar';
import SideBar from '../../components/SideBar';

// Views
import Betslip from '../Betslip';

// API
import { eventsAPI } from '../../api';

export class EventList extends React.Component<EventListProps, EventListState> {

  constructor(props: EventListProps) {
    super(props);
    
    this.state = {
        betslipVisible: false
    };

    this.toggleBetslip = this.toggleBetslip.bind(this);
  }

  componentDidMount() {
      eventsAPI.fetchEvents().then((events) => {
        if (this.props.fetchEvents) {
          this.props.fetchEvents(events);
        }
      });
    }

  componentWillReceiveProps(nextProps: EventListProps) {
      if (nextProps.selectionWasAdded) {
        this.setState({betslipVisible: nextProps.selectionWasAdded});
      }
  }

  toggleBetslip(value: boolean) {
    this.setState({betslipVisible: value});
  }

  render() {
    return (
      <div className="App">
        <SideBar 
          closeSideBar={this.toggleBetslip}
          visible={this.state.betslipVisible}
        >
          <Betslip />
        </SideBar>
        <NavBar menuClick={this.toggleBetslip} />
        {
          (this.props.eventsList) &&
          this.props.eventsList.map((value, key) => 
            (value.markets.length > 0) &&
              <Event key={value.id} data={value} />
          )
        }
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  const { eventsList, selectionWasAdded, addedSelections } = state;
  return {
      eventsList,
      selectionWasAdded, 
      addedSelections
  };
};

const mergeProps = (stateProps: Object, dispatchProps: Object, ownProps: Object) => {
  return Object.assign({}, ownProps, stateProps, dispatchProps);
};

export default connect(mapStateToProps, {
  fetchEvents: ActionCreators.FETCH_EVENTS.create}, 
                       mergeProps)(EventList);
