import * as React from 'react';

import Market from './Market';

class Event extends React.Component<EventProps> {

  render() {
    return (
      <div className="item-event">
        <div className="header">{this.props.data.name}</div>
        {
            (this.props.data.markets) &&
            this.props.data.markets.map((value, key) => 
             (value.selections.length > 0) &&
                <Market 
                  key={key} 
                  data={value} 
                />
            )
        }
      </div>
    );
  }
}

export default Event;
