import * as React from 'react';

import Selection from '../../../components/Selection';

class Market extends React.Component<MarketProps> {

  render() {

    const name = this.props.data.name.substr(this.props.data.name.indexOf(' '));

    return (
      <div className="market">
          <div className="title">
              <span>{name.charAt(1).toUpperCase() + name.slice(2)}</span>
          </div>
          <div className="selections">
              {
                (this.props.data.selections) &&
                this.props.data.selections.map((value, key) => 
                    <Selection 
                      key={key} 
                      data={value} 
                      selected={false} 
                      marketHeader={name} 
                    />
                )
              }
          </div>
      </div>
    );
  }
}

export default Market;
