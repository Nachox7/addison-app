import * as React from 'react';
import { connect } from 'react-redux';

import Selection from '../../components/Selection';

class Betslip extends React.Component<BetslipProps> {

  constructor(props: BetslipProps) {
    super(props);
  }

  render() {
    return (
      <div>
        <span />
        {
          (this.props.addedSelections) &&
          Array.from(this.props.addedSelections.values()).map((value, key) =>
              <Selection key={key} data={value.data} selected={true} marketHeader={value.marketHeader} />
          )
        }
      </div>
    );
  }
}

const mapStateToProps = (state: AppState) => {
  const { addedSelections } = state;
  return {
      addedSelections
  };
};

const mergeProps = (stateProps: Object, dispatchProps: Object, ownProps: Object) => {
  return Object.assign({}, ownProps, stateProps, dispatchProps);
};

export default connect(mapStateToProps, {}, mergeProps)(Betslip);
