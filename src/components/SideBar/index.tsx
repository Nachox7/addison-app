import * as React from 'react';

const SideBar: React.StatelessComponent<SideBarProps> = (props) => {

    const darkLayerClasses = (props.visible) 
      ? 'dark-layer_visible' 
      : 'dark-layer';
    const containerClasses = (props.visible) 
      ? 'container_visible' 
      : 'container';    

    return (
      <div className={darkLayerClasses}>
        <div className={containerClasses}>
          <div className="close-menu">
            <button 
              className="close fa fa-times fa-3x"
              onClick={() => props.closeSideBar(false)}
            />
          </div>
          {
              props.children
          }
        </div>
      </div>
    );
  };

export default SideBar;