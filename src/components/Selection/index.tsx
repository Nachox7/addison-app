import * as React from 'react';
import { connect } from 'react-redux';
import { ActionCreators } from '../../actions';

export class Selection extends React.Component<SelectionProps> {

    addSelection() {
        const selected = {
            data: this.props.data,
            marketHeader: this.props.marketHeader,
            selected: true
        };

        if (this.props.addSelection) {
            this.props.addSelection(selected);
        }
    }

    removeSelection() {
        if (this.props.removeSelection) {
            this.props.removeSelection(this.props.data.id);
        }
    }

    renderSelectionSelected() {
        const marketHeader = (this.props.marketHeader) ? this.props.marketHeader : '';

        return (
            <div className="item-market">
              <div className="header">
                {
                    this.props.data.name + marketHeader
                }
              </div>
              <div className="selection">{this.props.data.price}</div>
              <div>
                <button
                    onClick={() => this.removeSelection()}
                >
                    Delete
                </button>
              </div>
            </div>
          );
    }

    renderSelection() {
        let selectionClasses = 'selection';
        let onClickMethod = this.addSelection.bind(this);

        if (this.props.addedSelections && this.props.addedSelections.has(this.props.data.id)) {
            selectionClasses = 'selection-green';
            onClickMethod = this.removeSelection.bind(this);
        }

        return (
            <a 
                className={selectionClasses}
                onClick={() => onClickMethod()}
            >
                        <span>{this.props.data.name}</span>
                        <span>{this.props.data.price}</span>
            </a>
        );
    }

    render() {
        return (this.props.selected) 
        ? this.renderSelectionSelected()
        : this.renderSelection();
    }
}

const mapStateToProps = (state: AppState) => {
    const { addedSelections } = state;
    return {
        addedSelections
    };
  };

const mergeProps = (stateProps: Object, dispatchProps: Object, ownProps: Object) => {
    return Object.assign({}, ownProps, stateProps, dispatchProps);
  };

export default connect(mapStateToProps, {
    addSelection: ActionCreators.ADD_SELECTION.create,
    removeSelection: ActionCreators.REMOVE_SELECTION.create},  
                       mergeProps
)(Selection);
