import * as React from 'react';

const NavBar: React.StatelessComponent<NavBarProps> = (props) => {
    return (
      <div>
        <nav>
          <button 
            className="fa fa-bars fa-3x"
            onClick={() => props.menuClick(true)}
          />
        </nav>
      </div>
    );
};

export default NavBar;
