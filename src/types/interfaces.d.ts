/** Store state */
interface AppState {
	readonly eventsList: EventType[];
	readonly addedSelections: Map<string, SelectionAdded>;
	readonly selectionWasAdded: boolean;
}

/** Event Entity */
interface EventType {
	id: string,
	name: string,
	markets: MarketType[],
}

/** Market Entity */
interface MarketType {
	id: string,
	name: string,
	selections: SelectionType[],
}

/** Selection Entity */
interface SelectionType {
	id: string,
	name: string,
	price: number,
}

interface SelectionAdded {
	data: SelectionType;
	marketHeader?: string;
	selected: boolean;
}

/** Props */
interface BetslipProps{
	eventsList?: EventType[];
	addedSelections?: Map<string, SelectionAdded>;
	fetchEvents?: (event: EventType[]) => {};
	addSelections?: (selection: SelectionProps) => {};
}

interface EventListProps{
	eventsList?: EventType[];
	addedSelections?: Map<string, SelectionAdded>;
	selectionWasAdded?:boolean;
	fetchEvents?: (event: EventType[]) => {};
	addSelections?: (selection: SelectionProps) => {};
}

interface EventProps {
	data: EventType;
}

interface MarketProps {
	data: MarketType;
}

interface NavBarProps {
	menuClick: (visible: boolean) => void;
}

interface SelectionProps {
	data: SelectionType;
	marketHeader?: string;
	selected?: boolean;
	addedSelections?: Map<string, SelectionAdded>;
	addSelection?: (selection: SelectionAdded) => void;
	removeSelection?: (id: string) => void;
}

interface SideBarProps {
	visible: boolean;
	closeSideBar: (visible: boolean) => void;
}

/** States */
interface EventListState  {
	betslipVisible: boolean;
}
