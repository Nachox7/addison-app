import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import appStore from './store';

import 'normalize.css';
import './assets/scss/index.css';

// Views
import EventList from './views/EventList/';

ReactDOM.render(
  <Provider store={appStore}>
    <EventList />
  </Provider>,
  document.getElementById('root') as HTMLElement
);