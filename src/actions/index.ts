export class ActionCreator<T, P> {
    readonly type: T;
    readonly payload: P;

    constructor(type: T) { this.type = type; }
    create? = (payload: P) => ({ type: this.type, payload });
}

export const ActionCreators = {
    FETCH_EVENTS: new ActionCreator<'fetchEvents', EventType[]>('fetchEvents'),
    ADD_SELECTION: new ActionCreator<'addSelection', SelectionAdded>('addSelection'),
    REMOVE_SELECTION: new ActionCreator<'removeSelection', string>('removeSelection')
};

export type Action = typeof ActionCreators[keyof typeof ActionCreators];