import { createStore } from 'redux';
import reducer from './reducers';

export const initialState = {
    eventsList: [],
    addedSelections: new Map(),
    selectionWasAdded: false
};

const appStore = createStore<AppState>(reducer, initialState);

export default appStore;