import { eventsAPI } from '../api';

const isArrayEventType = (data: {}): data is EventType[] => {
  return data !== undefined; 
};

describe('API Calls', () => {
  it(' should returns data with the data structure expected', () => {
    eventsAPI.fetchEvents().then((evs) => {
      expect(isArrayEventType(evs)).toEqual(true);
    });
  });
});
