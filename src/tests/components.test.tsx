import * as React from 'react';

import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import AppStore from '../store';

import EventList from '../views/EventList';
import NavBar from '../components/NavBar';
import { Selection } from '../components/Selection';
import SideBar from '../components/SideBar';
  
describe('components', () => {
    describe('EventList', () => {
        const eventListWrapper = mount(
        <Provider store={AppStore}>
            <EventList />
        </Provider>);

        it('should make SideBar visible on NavBar button click', () => {
            expect(eventListWrapper.find(SideBar).props().visible).toBe(false);

            eventListWrapper.find(NavBar).find('button').simulate('click');

            expect(eventListWrapper.find(SideBar).props().visible).toBe(true);
        });
    });
    
    describe('Selection', () => {
        it('should render not selected Selection when selected false', () => {
            const props = {
                data: {
                    id: '1',
                    name: 'Test',
                    price: 2
                }, 
                marketHeader: 'Test',
                selected: false
            };
            const selectionWrapper = mount(<Selection {...props} />);

            expect(selectionWrapper).toMatchSnapshot();
            expect(selectionWrapper.find('a').hasClass('selection')).toBe(true);
        });

        it('should render selected Selection when selected false', () => {
            const props = {
                data: {
                    id: '1',
                    name: 'Test',
                    price: 2
                }, 
                marketHeader: 'Test',
                selected: true
            };
            const selectionWrapper = mount(<Selection {...props} />);

            expect(selectionWrapper).toMatchSnapshot();
            expect(selectionWrapper.find('button').exists()).toBe(true);
        });
    });
});
