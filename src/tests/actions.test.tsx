import { ActionCreators } from '../actions';
import { events } from '../api/mockData';

describe('actions', () => {
  it('should create an action to fetch events', () => {
    const payload: EventType[] = events;
    const expectedAction = {
      type: ActionCreators.FETCH_EVENTS.type,
      payload
    };
    if (ActionCreators.FETCH_EVENTS.create) {
      expect(ActionCreators.FETCH_EVENTS.create(payload)).toEqual(expectedAction);
    }
  });

  it('should create an action to add a Selection', () => {
    const payload: SelectionAdded = {
      data: {
        id: '1',
        name: 'Test',
        price: 1
      },
      marketHeader: '',
      selected: true
    };
    const expectedAction = {
      type: ActionCreators.ADD_SELECTION.type,
      payload
    };
    if (ActionCreators.ADD_SELECTION.create) {
      expect(ActionCreators.ADD_SELECTION.create(payload)).toEqual(expectedAction);
    }
  });

  it('should create an action to remove a Selection', () => {
    const payload: SelectionAdded = {
      data: {
        id: '1',
        name: 'Test',
        price: 1
      },
      marketHeader: '',
      selected: true
    };
    const expectedAction = {
      type: ActionCreators.REMOVE_SELECTION.type,
      payload: payload.data.id
    };
    if (ActionCreators.REMOVE_SELECTION.create) {
      expect(ActionCreators.REMOVE_SELECTION.create(payload.data.id)).toEqual(expectedAction);
    }
  });
});
