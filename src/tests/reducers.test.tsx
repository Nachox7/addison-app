import { ActionCreators } from '../actions';
import reducer from '../reducers';
import { initialState } from '../store';
import { events } from '../api/mockData';

describe('todos reducer', () => {

  it('should handle FETCH_EVENTS', () => {

    expect(
        reducer(initialState, {
          type: ActionCreators.FETCH_EVENTS.type,
          payload: events
        })
    ).toEqual(
        {
          addedSelections: new Map(),
          eventsList: events,
          selectionWasAdded: false
        }
      );
  });

  it('should handle ADD_SELECTION', () => {

    const selectionExample = {
        data: {
          id: '1',
          name: 'Test',
          price: 1
        },
        marketHeader: '',
        selected: true
      };

    expect(
      reducer(initialState, {
        type: ActionCreators.ADD_SELECTION.type,
        payload: selectionExample
      })
    ).toEqual(
      {
        addedSelections: new Map().set(selectionExample.data.id, selectionExample),
        eventsList: [],
        selectionWasAdded: true
      }
    );
  });

  it('should handle REMOVE_SELECTION', () => {
    
        const selectionExample = {
            data: {
              id: '1',
              name: 'Test',
              price: 1
            },
            marketHeader: '',
            selected: true
          };

        initialState.addedSelections.set(selectionExample.data.id, selectionExample);
        
        const afterState = initialState.addedSelections.set(selectionExample.data.id, selectionExample);
        
        afterState.delete(selectionExample.data.id);
    
        expect(
          reducer(initialState, {
            type: ActionCreators.REMOVE_SELECTION.type,
            payload: selectionExample.data.id
          })
        ).toEqual(
          {
            addedSelections: afterState,
            eventsList: [],
            selectionWasAdded: false
          }
        );
      });
});