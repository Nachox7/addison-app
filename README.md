Addison App
===================

This app follows an architecture based on React+Redux written in Typescript

----------

File tree
-------------

	 - actions -> Redux actions to dispatch
	 - api -> Api call to get events list
	 - assets -> SCSS and resources that we could need in our components
	 - components -> Independent and smart components that can be used in views
	 - reducers -> Redux reducers to call
	 - tests -> Unit testing for Redux, API and components
	 - types -> Interfaces where are defined all data structures used
	 - views -> Main containers. This have it's own components that will be used by them
		 - Smart Components
			 - index.tsx -> View container
			 - styles.scss -> SASS for container and it's components
			 - components -> Components that belongs to the view

----------

Technologies & Decisions

-------------------

**Architecture**: As I said is based on *React + Redux* written in *typescript*. No extra libraries were used for main development. I have been thinking about use Redux saga or similar for asynchronous calls and Lodash for collections management, but finally, I take advantage of *ES6* map and do a simple API call without any middleware. The map is necessary for an easy access when we want to have out selections somewhere without iterating over the whole data received. 

**Scalability**: I take serious the scalability for this app and I made a component of everything I consider that could be reused in futuro features. For example the component *"SideBar"*, allow us to show any content which is not essential to have in the main view. Also the architecture have an strict organisation to have as much as possible all ordered and accesible. That's why, the views have it's own components folder. Also with Redux I did the majority of components smart.

**Tests:** All the unit testing was made with the Facebook framework *Jest* and the use of *enzyme*. It is very confortable and has an easy setup.

**Assets**: *Font Awesome* used for the icons and *SASS* as main CSS framework and *normalize.css* library. The whole layout was made from scratch without any extra framework.

----------

How to run it
-------------------

 1.  Clone/Download git repository on a folder 
 2. Open a terminal and switch to the project directory `cd ./project_directory` 
 2. Then do `npm install` to install all the project dependencies
 3. Once we have installed dependencies, we only need to do `npm start`, it will automatically opens [http://localhost:3000](http://localhost:3000)
 4. Use the app!

To pass unit tests: ```npm test``` 